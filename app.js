/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint new-cap: 0 */
'use strict';

require('newrelic');

const bodyParser = require('body-parser');
const compression = require('compression');
const DDPServer = require('@sonar/sonar-ddp-handler');
const express = require('express');
const formidable = require('formidable');
const fs = require('fs');
const http = require('http');
const winston = require('winston');
const path = require('path');

const LeadsRetriever = require('./helpers/leads-retriever');

const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

const router = express.Router();
const app = express();

app.set('port', process.env.PORT || '3000');
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(compression());
app.use('/', router);

/**
 * HTTP handlers
 */
router.post('/bing', (req, res) => {
  const form = new formidable.IncomingForm();

  return new Promise((resolve, reject) => {
    form.parse(req, (err, fields) => {
      if (err) {
        reject({ error: { message: 'Invalid request', code: 13 } });
      }
      fields.keys = [fields.query, fields.keys];
      resolve(fields);
    });
  })
    .then(data => {
      const getLeads = new LeadsRetriever(data);
      return getLeads.grab();
    })
    .then(results => {
      res.status(!results.error ? 200 : 500).send(`<pre>${JSON.stringify(results, null, 2)}</pre>`);
    })
    .catch(() => {
      const text = '<pre>{ error: { message: "No leads retrieved", code: 10 }</pre>';
      res.status(500).send(text);
    });
});

router.get('/status', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/index.html'));
});

router.get('/test', (req, res) => {
  fs.readFile(path.join(__dirname, '/public/test.html'), (err, data) => {
    res.writeHeader(200, { 'Content-Type': 'text/html', 'Content-Length': data.length });
    res.write(data);
    res.end();
  });
});

/**
 * DDP methods
 */
app.server = http.createServer(app);
const server = new DDPServer({ httpServer: app.server });

server.methods({
  bing: function bing(req) {
    return new Promise((resolve) => {
      const getLeads = new LeadsRetriever(req);
      getLeads.grab()
        .then(data => resolve(data));
    });
  },
});

app.server.listen(app.get('port'), () => {
  logger.info(`BING SOOTHSAYER is up & running on port #${app.get('port')}`);
});

module.exports = app;
